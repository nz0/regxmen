# Regular Expression gives you Superpowers!

*This content is not entirely my own and is not for redistribution.*

---------
### Technical Sources:

http://teranetics.net/2018/05/13/regular-expressions-for-network-engineers/

regex101.com:
https://regex101.com/r/otyFk2/1

--------------------------
![Pattern Recognition](https://images.schoolspecialty.com/images/024962_ecommfullsize.jpg)

**This shape sorting cube toy for children is a lot like Regular Expressions. The different types of shaped holes are examples of META Characters!**

Meta Characters are like "Meta Humans"/Mutants with super powers to help you win.

![Meta Humans/Mutants](https://cdn2us.denofgeek.com/sites/denofgeekus/files/styles/main_wide/public/x-men_5.jpg?itok=auNp3Vva)

**.*+[]\^$|()?** - XMEN
___

![Gambit](https://vignette.wikia.nocookie.net/marvelanimated/images/a/ac/Gambit.jpg/revision/latest/scale-to-width-down/340?cb=20140613060021)

. Period
> "Pick a card, any card"

Ability: Match any single character

```
 "a.c" matches abc, acc, adc, a1c
```

*Gambit can deal the whole deck but plays **one** card at a time*

___

![Storm](https://i.pinimg.com/originals/13/44/67/134467508da7c1c3c444436ebde0422d.jpg)

\+  Plus

> "...I am Storm, and for me, there are no such things as limits."

Ability: Match preceeding character one or more times without limit.

```
"ab+c" matches abc, abbc, abbbc
```

*Storm is a great **addition** and team player whose power matches without limits.*
___

![Wolverine](https://sm.mashable.com/mashable_sea/photo/default/reasons-wolverine-returns-cover-1134827_w8rv.jpg)

\*  Asterisk

> "Hey Bub, I'm not finished with you yet"

Ability: Match preceeding character zero or more times

```
"ab*c" matches ac, abc, abbc, abbbc
```

*Wolverine is an **excessive wildcard** that adds to a given previous character, and if has to, **will do it alone**.*
___

![Jubilee](http://static02.mediaite.com/themarysue/uploads/2015/03/Jubilee.gif)

[ ] Square Brackets *and Special Ranges*

> "Wow, this might not be so bad after all"

Ability: Match any of the characters between the brackets.

```
“int Hu0/0/0/[1-3]” matches “int Hu0/0/0/1”, “int Hu0/0/0/2”, “int Hu0/0/0/3”
```
```
“int [a-Z]+1/0” matches “int GigabitEthernet1/0”, “int HundredGig1/0”, “int FastEthernet1/0”
```

*Jubilee can hack any interface by **combining ranges** of possible characters in brackets because her abilities vary a lot*

Special ranges include \d and \w and use of {}:

\d is the same as [0-9], or digits

\w is the same as [a-zA-Z0-9], or words

``` example
“int \w+/0” matches “int eth1/0”, “int gig1/0”, “int gig2/0”
```

{} Curley braces can be used to match a number of instances a previous pattern appears

```
"\d{1,3}" matches "1", "21", "999"
```

___
![Rogue](https://media0.giphy.com/media/zFJjHOghGrpgk/source.gif))

\ Backslash (Escaping Character)

> "This ain’t Cajun country, hun. Zip those lips."

Ability: Don't interpret the following single character as a regex Meta Character

Rogue can remove superpowers with a single touch.

```
"ab\*c" literally matches ab*c and does not interpret the metacharacter function.
```
___

![Cyclops](https://static3.srcdn.com/wordpress/wp-content/uploads/2019/09/Xmen-Cyclops-Shooting-Eye-Laser.jpg)

^ Caret/Circumflex/Exponent

>"To me, my Xmen"

Ability: Match the start of line

*Cyclops **leads** the X-MEN at the begining of the line always looking up.*

```
"^ntp" matches ntp server..., ntp peer ...
```
___

![Magneto](https://cdna.artstation.com/p/assets/images/images/013/405/178/large/sadece-kaan-x-men-magneto-02-march-2017.jpg?1539453687)

$ Dollar Sign

>"Peace was never an option"

Ability: Match the end of line

```
"[0-9.]+$" matches and ip address at the end of a configuration line and any other number

ip address 192.168.1.1, would match "192.168.1.1"

switchport access vlan 10, would match "10"
```

*Magneto opposite of Cyclops stands to defend the existance and extend the power of mutants at the end of the line no matter the **costs***

___
![Beast](https://cdn1us.denofgeek.com/sites/denofgeekus/files/styles/article_width/public/axbeast.jpg?itok=KsWd_w0-)

| Pipe

>"If you prick us, do we not bleed?"

Ability: Match one of many Logical OR conditions

```
“eth|gig” matches “int eth1/0”, “int gig1/0”

but not “int f1/0”
```
*Beast is **Logical** and can operate intelligently **OR** with brute force.*


___

![Jean Grey](https://www.comicbasics.com/wp-content/uploads/2017/09/Jean-Grey-1024x575.jpg)

() Parenthesis

>"When were you going to tell me that you're in love with me?"

Ability: Define subexpression that can be recalled later

```
“int (eth|gig|fe)1/0” matches “int eth1/0”, “int gig1/0”, “int fe1/0)

“int (eth|gig)([1-2])+/0” matches “int eth1/0”, “int eth2/0”, “int gig1/0”, “int gig2/0”
```

```
Example Text:   interface gig1
Search Pattern: interface ([a-z]+)(\d+)
\1 in the replace pattern will match contents of ([a-z]+) = gig

\2 in the replace pattern will match contents of (\d+) = 1


Example Text:   interface gig1
Search Pattern: interface ([a-z]+(\d+))
\1 in the replace pattern will match contents of ([a-z]+(\d+)) = gig1

\2 in the replace pattern will match contents of (\d+) = 1
```

*Jean Grey can read minds and **recall** expressions*
___

![ProfX](https://media2.giphy.com/media/aUKJ2ZkoJ3INW/source.gif)

? Question Mark

> "Just because someone stumbles and loses their way, it doesn't mean they're lost forever. Sometimes, we all need a little help."

Ability: Match a previous metacharacter zero or one time. ***If match does occur then it will include the match and ignore the "zero" match part.

```
abc(de)?f matches abcdef

but not “abcf” in the greedy mode if “abcdef” is the input string

```

*Professor X's true power is **when** to mind differences and ultimately when not to?*
___

# DANGER ROOM!
![DangerRoom](https://vignette.wikia.nocookie.net/marvelmovies/images/4/4a/Danger_Room.jpg/revision/latest?cb=20100828101937)

Get ready to test the powers of your meta characters in the Danger Room. We need a new network configuration for data two new vlans. One for data and one for voice.


## Source Data : *The blocks that go into your toy*

```
<vlan>,<name>,<desc>,<ip>,<mask>,<dhcp1>,<dhcp2>
10,data,Data VLAN,10.10.10.0,255.255.255.0,10.1.1.1,10.1.1.2
20,voice,Voice VLAN,10.10.20.0,255.255.255.0,10.1.1.1,10.1.1.2
```

1. Copy and paste this information into Notepad++

## Pattern: *The shapes and positions of the holes of your toy's side panel*

A pattern that you can use to match above:
> ^Cyclops, (Jean Grey), .Gambit, +Storm, $Magneto
```
^(.+),(.+),(.+),(.+),(.+),(.+),(.+)$
```
2. Press Ctrl+F to bring up Find and Replace, from the Replace tab copy and past the pattern above into the Find field.

## Template: *A side of your toy block you choose to put the pattern on*
```
vlan <VLAN>
 name <NAME>

interface vlan<VLAN>
 description <vlan-desc>
 ip address <ip> <mask>
 ip helper-address <helper1>
 ip helper-address <helper2>
 no shutdown
```

## Template converted to 'Replace' output: *Placing the blocks into the toy*
```
vlan \1\r\n name \2\r\n\r\ninterface vlan\1\r\n description \3\r\n ip address \4 \5\r\n ip helper-address \6\r\n ip helper-address \7\r\n no shutdown\r\n
```
3. From the same Find and Replace window in the Replace field copy and paste the above code snippet and hit Replace all.

## Noteable

\r is Carriage *return* sometimes referred to as CR

\n is *new* line sometimes referred to as LF or Line Feed.

\1 and other numbers are powers we are using from Jean Grey or the parenthises from defining subexpressions so that we can recall them.

Some programs use $1 instead of the \1 notation like VSCode and bash.

# Professor X: Accounting for missing configuration?

Some of our data is missing from our source data! Professor X reminds us to stay calm and he will provide his powers and guidance to help us.

## Source Data: Missing data
```
vlan 10
 name data

interface vlan10
 description Data VLAN
 ip address 10.10.10.0 255.255.255.0
 ip helper-address 10.1.1.1
 ip helper-address 10.1.1.2
 no shutdown

vlan 20
 name voice

interface vlan20
 ip address 10.10.20.0 255.255.255.0
 ip helper-address 10.1.1.1
 ip helper-address 10.1.1.2
 no shutdown
```
Notice Vlan 20 doesn't have a description line how can we work around this?

Copy this into Notepad so we can work with it anyway.

## Update our pattern to account for missing data

We will use Professor X (?)'s special powers to match if previous meta characters show up ZERO or one time, and when it does match at least one time also include that part of the result.

In the Find With field use:

```
vlan (\d+)\r\n name (\w+)\r\n\r\ninterface vlan\d+\r\n( description (.+)\r\n)? ip address ([0-9.]+) ([0-9.]+)\r\n ip helper-address ([0-9.]+)\r\n ip helper-address ([0-9.]+)\r\n no shutdown\r\n
```

In the Replace With:
```
\1,\2,\4,\5,\6,\7,\8
```

# WHAT IF?
![Whatif](https://upload.wikimedia.org/wikipedia/en/8/8a/Marvel%27s_What_If...%3F_logo.png)
![whatifcomic](https://www.screengeek.net/wp-content/uploads/2019/03/marvel-what-if.jpg)

## What if ... we have the configuration and want to output the tabulated data

Pattern to match configuration:

```
vlan (\d+)\r\n name (\w+)\r\n\r\ninterface vlan\d+\r\n description (.+)\r\n ip address ([0-9.]+) ([0-9.]+)\r\n ip helper-address ([0-9.]+)\r\n ip helper-address ([0-9.]+)\r\n no shutdown\r\n
```

Template to convert to replacement output:

```
\1,\2,\3,\4,\5,\6,\7
```

Which gets you back to your original data:

```
10,data,Data VLAN,10.10.10.0,255.255.255.0,10.1.1.1,10.1.1.2
20,voice,Voice VLAN,10.10.20.0,255.255.255.0,10.1.1.1,10.1.1.2
```


## What if ... we added more many more networks?!

You can open the csv file in Excel and drag down the rows to easily duplicate the data. Then open it in Notepad and apply the same method to create all the configuration at a better scale.


# Try it yourself - You may Google but have to explain it

1. How to match an ip address?

2. How to  match the ip address for the neighbor-group V4-THIS-TO-THAT and neighbor-group V4-THAT-TO-THIS and return just the ip addresses?

```
 neighbor 1.1.1.1
  use neighbor-group V4-THIS-TO-THAT
  description IPv4 iBGP to SOMETHING1
 !
 neighbor 1.1.1.2
  use neighbor-group V4-THAT-TO-THIS
  description IPv4 iBGP to SOMETHING2
 !
 neighbor 1.1.1.3
  use neighbor-group V4-THIS-TO-THIS
  description IPv4 iBGP to SOMETHING3
 !
```

3. How to clear out multiple spaces inside of a config line?
```
access-list 112 permit udp 192.168.1.1    any eq domain
access-list 112 permit udp any eq    domain any
access-list 112 permit    tcp any any eq domain
access-list 112 permit tcp any eq  domain     any
```


