# regXMEN

A fun introduction to Regular Expressions with XMEN cast. I do not claim to own
the rights of any of the content!:w

## Walkthrough

Use regXMEN.md for walk through along with Notepad++ and possibly regex101.com

### Technical Sources:

The following sources are used to come up with this content

http://teranetics.net/2018/05/13/regular-expressions-for-network-engineers/

https://regex101.com/r/otyFk2/1

